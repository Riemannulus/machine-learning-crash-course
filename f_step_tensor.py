from __future__ import print_function

import math
import sys

from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

def preprocess_features(california_housing_dataframe):
    selected_features = california_housing_dataframe[[
        'households'
        , 'population']]
    return selected_features

def preprocess_target(california_housing_dataframe):
    return california_housing_dataframe['total_rooms']

def my_input_fn(
        features
        , targets
        , batch_size=1
        , shuffle=True
        , num_epochs=None):
    # pandas 데이터를 np array dict로 변환하기

    features = {key: np.array(value) for key, value in dict(features).items()}

    # 데이터 세트를 만들고 배치 크기와 반복 횟수 설정
    ds = Dataset.from_tensor_slices((features, targets))
    ds = ds.batch(batch_size).repeat(num_epochs)

    if shuffle:
        ds = ds.shuffle(buffer_size=10000)

    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels

def construct_feature_columns(input_features):
    return set([tf.feature_column.numeric_column(my_feature)
        for my_feature in input_features])

def train_model(learning_rate
        , steps
        , batch_size
        , training_examples
        , training_targets
        , validation_examples
        , validation_targets
        , x1_feature='households'
        , x2_feature='population'):

    periods = 10
    steps_per_period = steps / periods

    training_input_fn = lambda:my_input_fn(
            training_examples
            , training_targets
            , batch_size=batch_size)
    predict_input_fn = lambda:my_input_fn(
            training_examples
            , training_targets
            , num_epochs=1, shuffle=False)
    predict_validation_input_fn = lambda:my_input_fn(
            validation_examples
            , validation_targets
            , num_epochs=1, shuffle=False)

    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)

    linear_regressor = tf.estimator.LinearRegressor(
            feature_columns = construct_feature_columns(training_examples)
            , optimizer = my_optimizer)

    print("Traning model...")
    print("RMSE (on training data):")
    root_mean_squared_errors = []

    for period in range (0, periods):
        linear_regressor.train(
                input_fn=training_input_fn
                , steps=steps_per_period)
        training_predictions = linear_regressor.predict(input_fn=predict_input_fn)
        training_predictions = np.array([item['predictions'][0] for item in training_predictions])
        training_predictions_RMSE = math.sqrt(
                metrics.mean_squared_error(training_predictions, training_targets))
        
        validation_predictions = linear_regressor.predict(input_fn=predict_validation_input_fn)
        validation_predictions = np.array([item['predictions'][0] for item in validation_predictions])
        validation_predictions_RMSE = math.sqrt(
                metrics.mean_squared_error(training_predictions, training_targets))

        model_weight_x1 = linear_regressor.get_variable_value(
                'linear/linear_model/%s/weights' % x1_feature)
        model_weight_x2 = linear_regressor.get_variable_value(
                'linear/linear_model/%s/weights' % x2_feature)
        model_bias = linear_regressor.get_variable_value(
                'linear/linear_model/bias_weights')

        print('    period %02d : training / %0.2f, validation / %0.2f'
                % (period, training_predictions_RMSE, validation_predictions_RMSE))
        print('        Weight by households : %0.2f' % model_weight_x1)
        print('        Weight by population : %0.2f' % model_weight_x2)
        print('        Bias : %0.2f' % model_bias)

    print("Model training finished.")

    return linear_regressor

def main(learning_rate_argv, steps_argv, batch_size_argv):
    tf.logging.set_verbosity(tf.logging.ERROR)
    pd.options.display.max_rows = 10
    pd.options.display.float_format = '{:.1f}'.format

    california_housing_dataframe = pd.read_csv(
            "https://dl.google.com/mlcc/mledu-datasets/california_housing_train.csv"
            , sep=",")

    california_housing_dataframe = california_housing_dataframe.reindex(
            np.random.permutation(california_housing_dataframe.index))

    training_examples = preprocess_features(california_housing_dataframe.head(12000))
    training_targets = preprocess_target(california_housing_dataframe.head(12000))

    print('Training describe : ')
    print(california_housing_dataframe.head(12000).describe())

    validation_examples = preprocess_features(california_housing_dataframe.tail(5000))
    validation_targets = preprocess_features(california_housing_dataframe.tail(5000))

    print('Validation describe : ')
    print(california_housing_dataframe.tail(5000).describe())

    linear_regressor = train_model(
            learning_rate=learning_rate_argv,
            steps=steps_argv,
            batch_size=batch_size_argv,
            training_examples = training_examples,
            training_targets = training_targets,
            validation_examples = validation_examples,
            validation_targets = validation_targets)

    print(linear_regressor.get_variable_names())
    
    x1 = pd.Series(['200.0', '300.0', '393.0'])
    x2 = pd.Series(['900.0', '2000.0', '100.0'])
  
if __name__ == '__main__':
    main(learning_rate_argv=0.001, steps_argv=1000, batch_size_argv=20)
